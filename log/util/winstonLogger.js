'use strict'

const LOGGER_KEY = Symbol.for("com.app.everset.logger");
var globalSymbols = Object.getOwnPropertySymbols(global);
var hasLog = (globalSymbols.indexOf(LOGGER_KEY) > -1);
var winston = require('winston');
var CloudWatchTransport = require('winston-aws-cloudwatch');
var initCount=0;
if (!hasLog) {

    global[LOGGER_KEY] = {


        format: function(text) {
            return ((new Date().toISOString() + " " + this.fileName + " : ") || " ") + text;
        },
        logger: winston,
        info: function(text) {

            if (!this.logger)
                throw new Error("Intialize the logger")
            this.logger.info(this.format(text))
        },
        error: function(text) {

            if (!this.logger)
                throw new Error("Intialize the logger")
            this.logger.error(this.format(text))
        },
        debug: function(text) {

            if (!this.logger)
                throw new Error("Intialize the logger")
            this.logger.debug(this.format(text))
        },
        warn: function(text) {

            if (!this.logger)
                throw new Error("Intialize the logger")
            this.logger.warn(this.format(text))
        }

    };
}
var init = function(groupName, streamName, region) {
    if(initCount===0)
    {
        global[LOGGER_KEY].logger.add(CloudWatchTransport, {
        logGroupName: groupName,
        logStreamName: streamName,
        createLogGroup: true,
        createLogStream: true,
        awsConfig: {
            region: region
        },
        formatLog: function(item) {
            return item.level + ': ' + item.message + ' ' + JSON.stringify(item.meta)
        }
    });
        initCount++;
    }
}

var getLogger = function(fileName) {
    if (!fileName)
        global[LOGGER_KEY].fileName = "";
    else {
        global[LOGGER_KEY].fileName = fileName
    }

    return global[LOGGER_KEY];
}
Object.freeze(getLogger);
exports.getLogger = getLogger;
exports.init = init;
