/**
 * Created by nikhil on 15/02/17.
 */
'use strict'

var AWS = require("aws-sdk");
var path = require('path');
var configenv = process.env.config;
var configPath = path.resolve(__dirname) + '/../config' + require(path.resolve(__dirname) + '/../config/secrete.js')(configenv)
AWS.config.loadFromPath(configPath);

var db = require('./dbUtil.js');
var winston = require('winston');
var CloudWatchTransport = require('winston-aws-cloudwatch');

var constants = require(rootPath+'/config/constants.json');

module.exports = {
    getTaskToken : function(executionId, activityArn, callback){
        winston.add(CloudWatchTransport, {
            logGroupName: 'JobWorkflow',
            logStreamName: executionId,
            createLogGroup: true,
            createLogStream: true,
            awsConfig: {
                region: AWSRegion
            },
            formatLog: function (item) {
                return item.level + ': ' + item.message + ' ' + JSON.stringify(item.meta)
            }
        });

        var dbReadTaskTokenParam= {
            "execution_id":executionId,
            "activity_arn":activityArn
        };
        var readParam= new db.createReadItem(dbReadTaskTokenParam,constants.taskTokenTableName);

        db.readItem(readParam,function(err,data){
           if(!err) {
               winston.info("Obtained task token successfully from DB : "+Item.task_token);
                callback(null,JSON.parse(data).Item.task_token);
           }else{
               winston.error("Error in getting task token. Returning error from callback.");
                callback(err,null);
           }
        });
    }
}