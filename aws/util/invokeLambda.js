'use strict'

var AWS = require("aws-sdk");
var path = require('path');
var rootPath = process.env.root_path;

var winstonLogger = require(rootPath+'/log/util/winstonLogger.js');
var logger;
module.exports = {
    asyncCallLambda: function(lambdaFunctionName, endpoint, payload, callback) {
        logger = winstonLogger.getLogger();
        AWS.config.update({
            endpoint: endpoint
        });
        var lambda = new AWS.Lambda();

        lambda.invoke({
            FunctionName: lambdaFunctionName,
            Payload: JSON.stringify(payload, null, 2),
            InvocationType: "Event",
            LogType: "Tail" // pass params
        }, function(error, data) {
            if (error) {
                logger.error("In invoke lambda utility, error received : "+error);
                callback(error, null)
            }
            if (data) {
                logger.info("Lambda accepted for invocation");
                callback(null, data.Payload)
            }
        });
    }
}
