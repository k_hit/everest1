/**
 * Created by nikhil on 23/02/17.
 */
var AWS = require("aws-sdk");
var path = require('path');
var configenv = process.env.config;
var rootPath = process.env.root_path;
var db = require(rootPath+'/storage/util/dbUtil.js');
var invokeLambda = require(rootPath+'/aws/util/invokeLambda.js');
var async = require('async');

var winstonLogger = require(rootPath+'/log/util/winstonLogger.js');
var logger;

var constants = require(rootPath+'/config/constants.json');

var pollTillCompletion = function(activityArn,executionId,dbEndpoint,AWSRegion,callback){
    logger.info("Inside poll till completion function. We will keep on polling till we obtain the task token from the DB.");
    var timesPolled = 0;
    async.forever(
        function(next){
            getTokenFromDB(activityArn,executionId,dbEndpoint, AWSRegion,function(err,data){
                if(err){
                    next("Error in polling from Database");
                }else{
                    if(data.length==0){
                        setTimeout(function() {
                            logger.info("No token received from DB. Waiting for a second before next polling");
                            timesPolled++;
                            if(timesPolled<120){
                                next();
                            }else{
                                next("Error in polling from Database. No result after 120 retries")
                            }
                        }, 1000);

                    }else{
                        logger.info("Found data, returning : "+data[0].task_token);
                        next(data[0].task_token);
                    }
                }
            });
        },
        function(err){
            var errorTestString = "Error in polling from Database";
            var errorTestExp = new RegExp(errorTestString);
            if(errorTestExp.test(err)){
                //Genuine error, returning err to the callback
                logger.error("Error in getting task tokens for the activity : "+activityArn);
                callback(err);
            }else{
                logger.info(err);
                logger.info("Task token obtained for the activity : "+activityArn);
                //Here err will be the data(task token) returned by the DB query. Returning task token to the callback.
                callback(null,err);
            }
        }
    );
}

var getTokenFromDB = function(activityArn,executionId,dbEndpoint, AWSRegion,callback){
    logger.info("Checking for task token in DB");
    var queryParams = {
        TableName : constants.taskTokenTableName,//rename to take the name from config, or make a constants file
        KeyConditionExpression: "#execution_id=:execution_id and #activity_arn=:activity_arn",
        ExpressionAttributeNames:{
            "#execution_id": "execution_id",
            "#activity_arn": "activity_arn"
        },
        ExpressionAttributeValues: {
            ":execution_id":executionId,
            ":activity_arn":activityArn
        }
    };
    db.queryItem(queryParams,dbEndpoint,AWSRegion,callback);
}

module.exports = {
    /*
        This function will first check if the task token is present in the DB or not. If present, it will return the task token.
        Otherwise it will call the lambda which will insert the token into DB and then keep on polling the DB every second till the token
        becomes available. Returns the token once it becomes available.
     */
    getTaskToken: function(activityArn,executionId,dbEndpoint,lambdaEndPoint, AWSRegion,callback){
        logger = winstonLogger.getLogger('getTaskTokenForActivityWorker');

        getTokenFromDB(activityArn,executionId,dbEndpoint,AWSRegion,function(err,data){
            if(err){
                logger.error("Error invoking DB");
                callback(err);
            } else{
                if(data.length>0){
                    //TODO : handle this more generically, e.g. if more than one elements is returned by the DB, throw an error.
                    callback(null,data[0].task_token);
                }else{
                    logger.info("Token not found. Now we will call lambda and poll the DB.")
                    var lambdaName = 'everest-dev-getActivityTaskToken';
                    //Invoking the lambda which will insert this activity's task token to the DB.
                    var payload = {
                        "activity_arn": activityArn
                    }
                    invokeLambda.asyncCallLambda(lambdaName,lambdaEndPoint,payload,function(err,data){
                        if (err) {
                            logger.error("Lambda callback: Error in invoking getActivityTaskToken Lambda.");
                        }else{
                            logger.info("Lambda callback: getActivityTaskToken Lambda invoked asynchronously");
                        }
                    });
                    logger.info("Lambda called asynchronously. Now we poll.");
                    pollTillCompletion(activityArn,executionId,dbEndpoint,AWSRegion,callback);
                }
            }
        });
    },

    deleteTaskToken: function(executionId,activityArn,dbEndPoint,AWSRegion,callback){
        logger = winstonLogger.getLogger('getTaskTokenForActivityWorker');

        logger.info("Deleting task token from DB");

        var deleteParams = {
            Key :{
                execution_id:executionId,
                activity_arn:activityArn
            },
            TableName : constants.taskTokenTableName,
        };
        db.deleteItem(deleteParams,dbEndPoint,AWSRegion,callback);
    }
}