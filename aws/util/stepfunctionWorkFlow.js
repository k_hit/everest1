"use strict";

var AWS = require("aws-sdk");
var path = require('path');


module.exports={


  startWorkflow:function(arn, stepFunctionEndPoint, input, name, callback) {

        AWS.config.update({
            endpoint: stepFunctionEndPoint
        });
        var stepfunctions = new AWS.StepFunctions({
            apiVersion: '2016-11-23'
        });

        var params = {
            stateMachineArn: arn,
            input: input,
            name: name


        }

        stepfunctions.startExecution(params, function(err, data) {
            if (err) callback(err, null) // an error occurred
            else callback(null, data); // successful response
        });

    }

}
