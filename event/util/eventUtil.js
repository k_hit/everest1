
'use strict';
var path=require('path');
var rootPath = process.env.root_path;
var configenv = process.env.config;
var event = require(rootPath + '/event/model/Event.js');
var db = require(rootPath + '/storage/util//dbUtil.js');
var resourceConfig = require(require(rootPath + "/config/configuration.js")(configenv));

var constants = require(rootPath + '/config/constants.json');


module.exports={

    createEvent:function(id,name,type,targetType,resourceId,currentState,payload,time,user,callback)
    {
        var eventObject=new event(id,name,type,targetType,resourceId,currentState,payload,time,user)
        var param=new db.createWriteItem(eventObject,constants.eventTableName);

        console.log('inserting event '+ JSON.stringify(eventObject))
        db.insertItem(param, resourceConfig.dynamodbEndPoint, resourceConfig.lambdaRegion, function(err, data) {

            if (err) {
                console.log('error inserting event');
                callback(err, null);
            }
            else {
                console.log('event inserted');
                callback(err, JSON.stringify(data));
            }
        })

    }


}
