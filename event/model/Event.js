'use strict'


class Event {

    constructor(id,name,type,targetType,resourceId,currentState,payLoad,time,user) {
      this.id=id;
      this.name=name;
      this.type=type;
      this.targetType=targetType;
      this.resourceId=resourceId;
      this.currentState=currentState;
      this.time=time;
      this.user=user;
      this.payLoad=payLoad;


    }


}

module.exports = Event;
