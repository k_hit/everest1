'use strict'


var EVENT_TYPE= {
             ACTION:"ACTION",
             ERROR:"ERROR"
}

Object.freeze(EVENT_TYPE);

module.exports=EVENT_TYPE;