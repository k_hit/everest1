'use strict'


var TRANSITION_EVENTS = {

            JOB_APPROVE:"JOB_APPROVE",
            JOB_PROCESS:"JOB_PROCESS",
            JOB_CLOSE:"JOB_CLOSE"

}


module.exports=TRANSITION_EVENTS;