'use strict'

var rootPath = process.env.root_path;
var transitionEvents = require(rootPath + '/job/constants/validTransitionEvents.js');
var EVENTS = JSON.parse(JSON.stringify(transitionEvents));;

EVENTS.START_WORKFLOW="START_WORKFLOW"
EVENTS.JOB_CREATE="JOB_CREATE"
EVENTS.START_WORKFLOW_ERROR="START_WORKFLOW_ERROR"
EVENTS.JOB_CREATE_ERROR="JOB_CREATE_ERROR"
EVENTS.JOB_APPROVE_ERROR="JOB_APPROVE_ERROR"
EVENTS.JOB_PROCESS_ERROR="JOB_PROCESS_ERROR"
EVENTS.JOB_CLOSE_ERROR="JOB_CLOSE_ERROR"

Object.freeze(EVENTS);
module.exports=EVENTS;