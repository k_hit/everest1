/**
 * Created by nikhil on 25/02/17.
 */
'use strict'

var AWS = require('aws-sdk');
const uuidV1 = require('uuid/v1');
var path = require('path');
var stepFunctions=new AWS.StepFunctions({
    apiVersion: '2016-11-23'
});
var configenv = process.env.config;
var rootPath = process.env.root_path;

var resourceConfig = require(require(rootPath + "/config/configuration.js")(configenv));
var dbEndpoint = resourceConfig.dynamodbEndPoint;
var lambdaEndPoint = resourceConfig.lambdaEndpoint;
var AWSRegion = resourceConfig.lambdaRegion;
var db=require(rootPath+'/storage/util/dbUtil.js');
var async = require('async');
var taskTokenUtil = require(rootPath+'/aws/util/getTaskTokenForActivityWorker.js')
var winstonLogger = require(rootPath+'/log/util/winstonLogger.js');
var logger;

var eventUtil = require(rootPath + '/event/util/eventUtil.js');
var events = require(rootPath + 'job/constants/validEvents.js');
var eventType = require(rootPath + 'event/eventType.js');

module.exports.createService = (event, context, callback) => {
    var groupName = 'Workflow';

    
    var params = event;

    params.id= params.executionId;
    delete params.executionId;
    var param = new db.createWriteItem(params, "Job")
    var jobId=params.id;
    var user = event.user;
    var payload = event.payload;
    var currentState = event.currentState;
    
    var inputJobId = {
        "executionId": params.id
    }

    winstonLogger.init(groupName,jobId,AWSRegion);

    logger = winstonLogger.getLogger();
    logger.info(event);

    logger.info("Entering the createJobService ActionHandler");

     db.insertItem(param, resourceConfig.dynamodbEndPoint, resourceConfig.lambdaRegion, function(err, data) {

        if (err)
        {

             eventUtil.createEvent(uuidV1(), events.JOB_CREATE_ERROR,eventType.ERROR,'JOB',jobId,currentState,payload,
                new Date().toISOString,user, function (error, eventdata) {
                    if (error){
                        logger.error(error);
                        logger.info('Error event failed')
                        callback(error);
                    }
                    else{
                        logger.info(eventdata);
                        callback(err,eventdata);
                    }
                });


           
        }
        else {
            logger.info('Job Resource persisted successfully')

             eventUtil.createEvent(uuidV1(), events.JOB_CREATE,eventType.ACTION,'JOB',jobId,currentState,payload,
                new Date().toISOString,user, function (error, eventdata) {
                    if (error){
                        logger.error(error);
                        callback(error);
                    }else{
                        logger.info(eventdata);
                        callback(null, inputJobId)
                    }
                });


        
        }
    })





}




