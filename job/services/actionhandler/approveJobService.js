/**
 * Created by nikhil on 25/02/17.
 */
'use strict'

var AWS = require('aws-sdk');
const uuidV1 = require('uuid/v1');
var path = require('path');
var stepFunctions=new AWS.StepFunctions({
    apiVersion: '2016-11-23'
});
var configenv = process.env.config;
var rootPath = process.env.root_path;

var resourceConfig = require(require(rootPath + "/config/configuration.js")(configenv));
var dbEndpoint = resourceConfig.dynamodbEndPoint;
var lambdaEndPoint = resourceConfig.lambdaEndpoint;
var AWSRegion = resourceConfig.lambdaRegion;
var db=require(rootPath+'/storage/util/dbUtil.js');
var taskTokenUtil = require(rootPath+'/aws/util/getTaskTokenForActivityWorker.js')
var winstonLogger = require(rootPath+'/log/util/winstonLogger.js');

var logger;

var eventUtil = require(rootPath + '/event/util/eventUtil.js');
var events = require(rootPath + 'job/constants/validEvents.js');
var eventType = require(rootPath + 'event/eventType.js');

module.exports.approveService = (event, context, callback) => {
    var groupName = 'Workflow';
    var jobId = event.body.executionId;
    var user = event.body.user;
    var payload = event.body.payload;
    var currentState = 'JOB_APPROVED';

    winstonLogger.init(groupName,jobId,AWSRegion);

    logger = winstonLogger.getLogger();
    logger.info(event);
    logger.info("Entering the approveJobService ActionHandler");

    var taskToken;
    logger.info(jobId);
    persistResourceApprovedStateChange(jobId,function(err,data){
            if(err){
                logger.error("Error in persisting state change for the reosurce in DB");
                eventUtil.createEvent(uuidV1(), events.JOB_APPROVE_ERROR,eventType.ERROR,'JOB',jobId,currentState,payload,
                new Date().toISOString,user, function (error, eventdata) {
                    if (error){
                        logger.error(error);
                        callback(error);
                    }
                    else{
                        logger.info(eventdata);
                        callback(null,eventdata);
                    }
                });
            }else{
                logger.info("Resource State change persisted to DB");
                eventUtil.createEvent(uuidV1(), events.JOB_APPROVE,eventType.ACTION,'JOB',jobId, currentState,payload,  new Date().toISOString,
                user, function (error, eventdata) {
                    if (error){
                        logger.error(error);
                        callback(error);
                    }else{
                        logger.info(eventdata);
                        callback(null,eventdata);
                    }
                });
            }
    });
};

var persistResourceApprovedStateChange= function(jobId,callback){
    logger.info("in Persist function of Action Handler.");

    var updateParams = {
        Key: {
            id:jobId
        },
        ExpressionAttributeNames: {
            "#s": "status"
        },
        ExpressionAttributeValues: {
            ":s": "Approved"
        },

        ReturnValues: "ALL_NEW",
        TableName: "Job",
        UpdateExpression: "SET #s = :s"
    };
    db.updateItem(updateParams,dbEndpoint,AWSRegion,callback);
};