'use strict'

var path = require('path');
const uuidV1 = require('uuid/v1');

var configenv = process.env.config;
var rootPath = process.env.root_path
var step = require(rootPath + '/aws/util/stepfunctionWorkFlow.js')
var resourceConfig = require(require(rootPath + "/config/configuration.js")(configenv));

var dbEndpoint = resourceConfig.dynamodbEndPoint;
var AWSRegion = resourceConfig.lambdaRegion;
var constants = require(rootPath + '/config/constants.json');

var db = require(rootPath + '/storage/util/dbUtil.js');
var eventUtil = require(rootPath + '/event/util/eventUtil.js')
var event = require(rootPath + '/job/constants/validEvents.js')
var eventType = require(rootPath + '/event/eventType.js');

//handler to start the execution of job workflow and persist the jobId and related details

module.exports.startExecution = (event, context, callback) => {


    var jobId = uuidV1();

    var params = event.body;


    params.executionId = jobId;
    params.currentState = 'JOB_CREATED';

    var asyncTasks = [];
    var jobStateMachineArn = "";
    var keyname = "JobWorkFlowStage1"



    var queryParams = {
        TableName: constants.metadata,
        KeyConditionExpression: "#key=:key ",
        ExpressionAttributeNames: {
            "#key": "key"
        },
        ExpressionAttributeValues: {
            ":key": keyname,

        }
    };
    db.queryItem(queryParams, dbEndpoint, AWSRegion, function (err, data) {

        if (err)
            callback(err)
        else {
            jobStateMachineArn = data[0].value;

            step.startWorkflow(jobStateMachineArn, resourceConfig.jobStateMachineEndPoint, JSON.stringify(params), jobId, function (err, data) {
                if (err) {
                    console.log('Job workflow failed to start');
                    console.log(JSON.stringify(err));
                    eventUtil.createEvent(uuidV1(), event.START_WORKFLOW_ERROR, eventType.ERROR, event.body.targetType, jobId, "START_STATE", event.body.payload, new Date().toISOString, event.body.user, function (error, eventdata) {


                        if (error)
                            console.log(error)
                        else
                            console.log(eventdata)
                    })

                    callback(err, null);

                } else {

                    console.log('Job workflow started');

                    eventUtil.createEvent(uuidV1(), event.START_WORKFLOW, eventType.ACTION, event.body.targetType, jobId, "START_STATE", event.body.payload, new Date().toISOString, event.body.user, function (error, eventdata) {


                        if (error)
                            console.log(error)
                        else
                            console.log(eventdata)
                    })
                    callback(null, data);


                }
            });
        }

    });



};