'use strict'

var AWS = require('aws-sdk');
var path = require('path');
var stepFunctions = new AWS.StepFunctions({
    apiVersion: '2016-11-23'
});
var configenv = process.env.config;
var rootPath = process.env.root_path;

var resourceConfig = require(require(rootPath + "/config/configuration.js")(configenv));
var dbEndpoint = resourceConfig.dynamodbEndPoint;
var lambdaEndPoint = resourceConfig.lambdaEndpoint;
var AWSRegion = resourceConfig.lambdaRegion;
var db = require(rootPath + '/storage/util/dbUtil.js');
var constants = require(rootPath + '/config/constants.json');
var async = require('async');
var taskTokenUtil = require(rootPath + '/aws/util/getTaskTokenForActivityWorker.js')
var winstonLogger = require(rootPath + '/log/util/winstonLogger.js');

var logger;
var validEvents = require(rootPath + '/job/constants/validEvents.js');
var validTransitionEvents = require(rootPath + '/job/constants/validTransitionEvents.js');
var validEventTypes = require(rootPath + '/event/eventType.js');

module.exports.transitionService = (event, context, callback) => {
    var groupName = 'Workflow';
    var executionId;
    var currentState;
    var activity;
    var eventName;
    var eventType;

    winstonLogger.init(groupName, executionId, AWSRegion);

    logger = winstonLogger.getLogger();
    logger.info("Entering the transitionService function");
    logger.info(event);
    logger.info(JSON.stringify(event));
    
    //TODO: check that there is only one record in event.Records, otherwise throw error.
    //Loop for all the recieved records and return if it is not one of the valid trasition event
   var validEvent=false;
    event.Records.forEach((record) => {

        
        eventName = getFieldFromDynamoDb(record, "name");
        logger.info("Event Recieved "+eventName )
        logger.info("check if it is a valid or not "+ validTransitionEvents[eventName])

        if (!validTransitionEvents[eventName] ){
            logger.info("Invalid type of event. Exiting");
            return ;
        }
       

       validEvent=true;
        executionId = getFieldFromDynamoDb(record, "resourceId");
        currentState = getFieldFromDynamoDb(record, "currentState");

        eventType = getFieldFromDynamoDb(record, "type");
        logger.info("Reading from records");
        logger.info(executionId + "  " + currentState + " " + eventName + " " + eventType);

        // executionId=event.resourceId;
        // currentState=event.currentState;
        // eventName=event.name;

        // eventType=event.type;
        logger.info(executionId + "  " + currentState + " " + eventName + " " + eventType);
        if (!validEvents.hasOwnProperty(eventName)) {
            logger.info("The event " + eventName + " is not a state transition event. Exiting the lambda");
            //context.succeed();
            return null;
            logger.info("After context succeed");
        };
        var asyncTasks = [];
        var taskToken;
        asyncTasks.push(function (callback) {
            getActivityArnForState("JOB", currentState, dbEndpoint, AWSRegion, function (err, data) {
                if (err) {
                    logger.error("Error in getting the activity arn for the given resource and state");
                    callback(err);
                } else {
                    logger.info(JSON.stringify(data[0]));
                    activity = data[0].activity_arn;
                    callback(null, activity);
                }
            });
        });
        asyncTasks.push(function (callback) {
            taskTokenUtil.getTaskToken(activity, executionId, dbEndpoint, lambdaEndPoint, AWSRegion, function (err, data) {
                if (err) {
                    logger.error("Error in getting task token");
                    //TODO: Send an error callback
                    callback(err);
                } else {
                    taskToken = data;
                    logger.info("Task token --- ");
                    logger.info(taskToken);
                    callback(null, data);
                }
            });
        });
        asyncTasks.push(function(callback){
            sendTaskStatus(eventType,taskToken,activity, executionId, function(err,data){
                if(err){
                    logger.error("Error in sending task success/failure to state machine");
                    logger.error(JSON.stringify(err));
                    callback(err);
                }else{
                    logger.info("Task token sent to step function successfully");
                    logger.info(JSON.stringify(data));
                    callback(null,data);
                }
            });
        })
        async.series(asyncTasks, function (err, data) {
            if (err) {
                logger.error("Error in getting task token and sending to step function");
                context.succeed();
                callback(err);
            } else {
                logger.info("Task completed");
                context.succeed();
                callback(null,data);
            }
        })
    });

if(!validEvent)
    context.succeed();
};

var sendTaskStatus = function(eventType,taskToken,activity, executionId, callback){
    if (eventType === validEventTypes.ACTION) {
        logger.info("Sending task success to step function. Transitioning to next state");
        sendTaskSuccess(taskToken, activity, executionId, callback);
        //callback(null, data);
    } else if (eventType === validEventTypes.ERROR) {
        logger.info("Sending task failure to step function. Transitioning to fallback state");
        sendTaskFailure(taskToken, activity, executionId, err, callback);
        //callback(null, data);
    }
};

var getActivityArnForState = function (resourceType, currentState, dbEndpoint, AWSRegion, callback) {

    logger.info("Getting Activity Arn for "+ resourceType+ " "+ currentState);
    var queryParams = {
        TableName: constants.statesActivityArnMappingTableName,
        KeyConditionExpression: "#current_state=:current_state and #resource_type=:resource_type",
        ExpressionAttributeNames: {
            "#resource_type": "resource_type",
            "#current_state": "state"
        },
        ExpressionAttributeValues: {
            ":resource_type": resourceType,
            ":current_state": currentState
        }
    };
    db.queryItem(queryParams, dbEndpoint, AWSRegion, callback);
};

var sendTaskSuccess = function (taskToken, activityArn, executionId, callback) {
    logger.info("Task token in called function");
    logger.info(taskToken);
    var status = {
        executionId: executionId
    };
    status = JSON.stringify(status, null, 2);
    var taskSuccessRequestParams = {
        output: status,
        taskToken: taskToken
    };
    stepFunctions.sendTaskSuccess(taskSuccessRequestParams, function (err, data) {
        if (err) {
            logger.error("Error in sending task success to step machine");
            logger.error(err);
            logger.error(err.stack);
            callback(err);
        } else {
            logger.info("step Function successful. Deleting token from DB");
            taskTokenUtil.deleteTaskToken(executionId, activityArn, dbEndpoint, AWSRegion, callback);
        }
    });
};

var sendTaskFailure = function (taskToken, activityArn, executionId, error, callback) {
    var taskFailureRequestParams = {
        cause: 'Task failed. Fail execution',
        error: error,
        task: taskToken
    };
    stepFunctions.sendTaskFailure(taskFailureRequestParams, function (err, data) {
        if (err) {
            logger.error("Error in sending task failure to step machine");
            callback(err);
        } else {
            logger.info("step Function failure passed to Step machine. Deleting task token from DB.");
            taskTokenUtil.deleteTaskToken(executionId, activityArn, dbEndpoint, AWSRegion, callback);
        }
    });
};

var getFieldFromDynamoDb = function (record, fieldName) {
    logger.info(JSON.stringify(record.dynamodb));
    if(record["dynamodb"]["NewImage"][fieldName]){
        return record["dynamodb"]["NewImage"][fieldName]["S"];
    }else{
        return "";
    }
}