'use strict'

var path = require('path');
const uuidV1 = require('uuid/v1');

var configenv = process.env.config;
var rootPath = process.env.root_path;
var resourceConfig = require(require(rootPath + "/config/configuration.js")(configenv));
var db = require(rootPath + '/storage/util/dbUtil.js');
var constants = require(rootPath + '/config/constants.json');




function getAllStateFromWorkFlow(workflow) {
    return workflow.states;
}

function getAllTransitionFromWorkFlow(workflow)
{
    return workflow.transitions;
}

function getAllActionForState(workflow,state)
{

    var validActions=[];
   
    
    for(var i in workflow.actions)
    {
        
        for(var j in workflow.actions[i].validStates)
        {
            
            if(state===workflow.actions[i].validStates[j])
            {
                validActions.push(workflow.actions[i].name);
                break;

            }
        }
    }

    return validActions;
}


module.exports = {


      getAllTransitions :function ( event, context, callback) {

         
        var id = event.path.id;
       

        var param = {
            id: id
        }

        var param = new db.createReadItem(param, constants.workFlowTableName);

        db.readItem(param, resourceConfig.dynamodbEndPoint, resourceConfig.lambdaRegion, function (err, data) {

            if (err)
                callback(err)
            else {
                var result = JSON.parse(data);

                try {

                    var transitions=getAllTransitionFromWorkFlow(result.Item.workflow)
                    callback(null, transitions)
                } catch (e) {
                    callback(e)

                }
            }

        })


    },


    getAllStates: function (event, context, callback) {
       
  
        var id = event.path.id;
       

        var param = {
            id: id
        }

        var param = new db.createReadItem(param, constants.workFlowTableName);

        db.readItem(param, resourceConfig.dynamodbEndPoint, resourceConfig.lambdaRegion, function (err, data) {

            if (err)
                callback(err)
            else {
                var result = JSON.parse(data);

                try {

                    var states=getAllStateFromWorkFlow(result.Item.workflow)
                    callback(null, states)
                } catch (e) {
                    callback(e)

                }
            }

        })


    },
    getActionForState: function (event, context, callback) {
      
  
        var id = event.path.id;
       
        var stateName=event.path.state;


        var param = {
            id: id
        }

        var param = new db.createReadItem(param, constants.workFlowTableName);
        //console.log()
        db.readItem(param, resourceConfig.dynamodbEndPoint, resourceConfig.lambdaRegion, function (err, data) {

            if (err)
                callback(err)
            else {
                var result = JSON.parse(data);

                try {

                    var actions = getAllActionForState(result.Item.workflow, stateName)
                    callback(null, actions)
                } catch (e) {
                    callback(e)

                }
            }

        })


    }
}
