/**
 * Created by nikhil on 17/02/17.
 */
'use strict';
var path = require('path');
var configenv = process.env.config;
var rootPath = process.env.root_path;

var resourceConfig = require(require(rootPath + "/config/configuration.js")(configenv));

var dbEndpoint = resourceConfig.dynamodbEndPoint;
var lambdaEndpoint = resourceConfig.lambdaEndpoint;
var AWSRegion = resourceConfig.lambdaRegion;
var db=require(rootPath+'/storage/util/dbUtil.js');
var constants = require(rootPath+'/config/constants.json');
var async = require('async');
var invokeLambda = require(rootPath+'/aws/util/invokeLambda.js');
var winstonLogger = require(rootPath+'/log/util/winstonLogger.js');
var logger;

module.exports.getAllActivitiesTaskToken = (event,context,callback) => {
    var groupName = 'TaskTokenModule';
    var streamName = 'fetchTaskTokenFromStepMachineStream';
    winstonLogger.init(groupName,streamName,AWSRegion);

    logger = winstonLogger.getLogger('getActivityTaskToken');
    logger.info("Inside get All Activities lambda");
    invokeLambdaForEachActivity(function(err,data){
        var response;
        if(!err){
            logger.info("202 accepted status received for lambda invoke. Lambdas will be invoked asynchronously");
            response = {
                statusCode: 200,
                body: JSON.stringify({
                    message: 'Obtained all activities and called lambda for each activity'
                })
            };
            callback(null,response);
        }else {
            logger.error("Error:" + err);
            logger.error("Error in calling task token lambda for all activities");
            response = {
                statusCode: 500,
                body: JSON.stringify({
                    message: 'Error in calling task token lambda for all activities due to the error:' + err
                })
            };
            callback(err, response);
        }
    });
};


var invokeLambdaForEachActivity = function(callback) {
    var params = {
        TableName: constants.activityArnTableName
    };
    db.scanTable(params, dbEndpoint, AWSRegion, function(err,data){
       if(err){
           logger.info(err);
           callback(err);
       }else{
           var numActivities = data.length;
           // Using async queue to call all lambda invocations asynchronously.
           var q = async.queue(function(task,callback){
               invokeLambda.asyncCallLambda(task.lambdaName,task.lambdaEndPoint,task.activity_arn,callback);
           },numActivities);
           // Adding callback to be called on success of all lambda invocations.
           q.drain = function() {
               logger.info("getActivityTaskToken Lambda invoked asynchronously");
               callback(null,numActivities);
           };
           for (var i = 0; i < data.length; i++) {
               var obj = {
                   lambdaName : 'everest-dev-getActivityTaskToken',
                   lambdaEndPoint : lambdaEndpoint,
                   activity_arn: data[i],
               };
               q.push(obj, function(err,data) {
                   if (err) {
                       logger.error("Error in invoking getActivityTaskToken Lambda.");
                       callback(err, null);
                   }else{
                       logger.info("getActivityTaskToken Lambda invoked asynchronously");
                   }
               });
           }
       }
    });

};


