/**
 * Created by nikhil on 04/02/17.
 */
'use strict'
var AWS = require('aws-sdk');
var path = require('path');
var stepFunctions=new AWS.StepFunctions({
    apiVersion: '2016-11-23'
});
var configenv = process.env.config;
var rootPath = process.env.root_path;

var resourceConfig = require(require(rootPath + "/config/configuration.js")(configenv));
var endpoint = resourceConfig.dynamodbEndPoint;
var AWSRegion = resourceConfig.lambdaRegion;

var db=require(rootPath+'/storage/util/dbUtil.js');
var async = require('async');
var winstonLogger = require(rootPath+'/log/util/winstonLogger.js');
var constants = require(rootPath+'/config/constants.json');

var logger;

module.exports.getTaskToken = (event, context, callback) => {
    var groupName = 'TaskTokenModule';
    var streamName = 'fetchTaskTokenFromStepMachineStream';
    winstonLogger.init(groupName,streamName,AWSRegion);

    logger = winstonLogger.getLogger('getActivityTaskToken');

    logger.info("Entering the getTaskToken function");
    var activity= event.activity_arn;
    //var activity= "arn:aws:states:ap-northeast-1:764087563656:activity:get-greeting-activity";
    logger.info("activity populated. Activity is : "+activity);
    var params = {
        activityArn:activity,
        workerName: constants.taskTokenActivityWorker
    };
    getStepFunctionActivityTaskToken(params, function(err,data){
        var response;
        if(!err){
            logger.info("Total number of tasks obtained : "+data);
            response = {
                statusCode: 200,
                body: JSON.stringify({
                    message: 'Obtained all task tokens and inserted them into the DB successfully for the activity '+params.activityArn
                })
            };
            callback(null,response);
        }else{
            logger.error(err);
            logger.error("Error in obtaining all task tokens for the activity:"+params.activityArn);
            response = {
                statusCode: 500,
                body: JSON.stringify({
                    message: 'Error in obtaining all task tokens for the activity:"'+params.activityArn+'due to the error:'+err
                })
            };
            callback(err,response);
        }
    });
};

var getStepFunctionActivityTaskToken = function(params, callback){
    var totalTasksObtained = 0;
    logger.info("inside getStepFunctionActivityTaskToken");
    async.forever(
        function(next){
            getStepFunctionActivityTask(next,params);
            totalTasksObtained++;
        },
        function(err){
            var errorTestString = "Error in getting task tokens";
            var errorTestExp = new RegExp(errorTestString);
            if(errorTestExp.test(err)){
                logger.error("Error in getting task tokens for the activity : "+params.activityArn);
                callback(err);
            }else{
                logger.info(err);
                logger.info("No more tasks scheduled for the activity : "+params.activityArn);
                callback(null,totalTasksObtained-1);
            }
        }
    );
};

var getStepFunctionActivityTask=function(next,params){
    logger.info("Inside getStepFunctionActivityTask method");
    stepFunctions.getActivityTask(params, function(err, data){
        var activityResult;
        var taskToken;
        var executionId;
        if(err) {
            logger.error("Error callback");
            logger.error(err);
            next(err+"Error in getting task tokens");
        }
        else {
            logger.info("Data callback");
            activityResult=data;
            taskToken=activityResult.taskToken;
            logger.info("Task token obtained : "+taskToken);
            if(!taskToken){
                next("No more tasks scheduled for activity.");
            }else{
                executionId=JSON.parse(activityResult.input).executionId;
                var dbInsertTaskTokenParam= {
                    "execution_id":executionId,
                    "activity_arn":params.activityArn,
                    "task_token":taskToken
                };
                logger.info("Going to asynchronously insert task token in the database: "+dbInsertTaskTokenParam.toString());
                next();// Calling the next iteration. Asynchronously, we will be inserting the values obtained in DB
                var createParam= new db.createWriteItem(dbInsertTaskTokenParam,constants.taskTokenTableName);
                db.insertItem(createParam, endpoint, AWSRegion, function(err,data){
                    if(err){
                        logger.error("Error in persisting task_token to the DB due to the error: "+err);
                        logger.error(err.stack);
                        logger.error("Returning back failed task to the step machine for the task :"+taskToken);
                        var taskFailureParams = {
                            taskToken: taskToken,
                            cause: 'Unable to persist the task token in the DB. Returning error to the step function',
                            error: err.message
                        };
                        stepFunctions.sendTaskFailure(taskFailureParams, function(err, data) {
                            if (err){
                                logger.error(err);
                            }
                            else {
                                logger.info("Sent failure to the step function for the task : "+taskToken, " Return : "+data);
                            }
                        });
                    }else{
                        logger.info("Inserted item : "+data);
                    }
                });
            }

        }
    });
}
