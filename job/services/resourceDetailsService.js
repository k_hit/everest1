/**
 * Created by nikhil on 26/02/17.
 */
'use strict'

var path = require('path');
var configenv = process.env.config;
var rootPath = process.env.root_path;

var resourceConfig = require(require(rootPath + "/config/configuration.js")(configenv));
var dbEndpoint = resourceConfig.dynamodbEndPoint;
var AWSRegion = resourceConfig.lambdaRegion;
var dbUtil=require(rootPath+'/storage/util/dbUtil.js');
var winstonLogger = require(rootPath+'/log/util/winstonLogger.js');

var constants = require(rootPath+'/config/constants.json');

var logger;

module.exports.getResourceDetails = (event, context, callback) => {
    var groupName = 'Workflow';
    var resourceId = event.body.resourceId;
    winstonLogger.init(groupName,resourceId,AWSRegion);

    logger = winstonLogger.getLogger();
    var param = {
        id:resourceId
    }
    logger.info(constants.jobTableName);
    var param = new dbUtil.createReadItem(param,constants.jobTableName);
    dbUtil.readItem(param,dbEndpoint,AWSRegion,function(err,data){
        if(err){
            logger.error("Error in getting resource from DB");
            logger.error(err);
            callback(err);
        }else{
            logger.info("Resource obtained");
            logger.info(JSON.stringify(JSON.parse(data).Item));
            callback(null,JSON.parse(data).Item);
        }
    })
}