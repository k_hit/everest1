'use strict'


var path = require('path');
var configenv = process.env.config;
var rootPath = process.env.root_path;
var resourceConfig = require(require(rootPath + "/config/configuration.js")(configenv));
var constants = require(rootPath+'/config/constants.json');
var db = require(rootPath+'/storage/util/dbUtil.js');

//handler to start the execution of job workflow and persist the jobId and related details
module.exports = {

    readEvents: function(event, context, callback) {

        var param = {
            id: event.id

        }

        var param = new db.createReadItem(param, constants.eventTableName);
        //console.log()
        db.readItem(param, resourceConfig.dynamodbEndPoint, resourceConfig.lambdaRegion, function(err, data) {

            callback(err, data)
        })
    },
    readResourceEvents: function(event, context, callback) {

        var queryParams = {
            TableName: constants.eventTableName,
            FilterExpression: "#resourceId = :resourceId",
            ExpressionAttributeNames: {
                "#resourceId": "resourceId"
            },
            ExpressionAttributeValues: {
                ":resourceId": event.resourceId
            }
        };

        var item = db.scanTable(queryParams, resourceConfig.dynamodbEndPoint, resourceConfig.lambdaRegion, function(err, data) {

            callback(err, data)
        });
    }




}
