'use strict'
var AWS = require("aws-sdk");
var path = require('path');
var rootPath = process.env.root_path;
var winstonLogger = require(rootPath+'/log/util/winstonLogger.js');
var logger;

module.exports = {
    createReadItem: function(param, tablename) {
        this.TableName = tablename;
        this.Key = param;
    },
    createWriteItem: function(param, tablename) {
        this.TableName = tablename;
        this.Item = param;
    },

    insertItem: function(params, endpoint, region, callback) {
        logger = winstonLogger.getLogger();
        logger.info("Adding a new item...");
        AWS.config.update({
            endpoint: endpoint,
            region: region
        });
        var docClient = new AWS.DynamoDB.DocumentClient();
        docClient.put(params, function(err, data) {
            if (err) {
                callback(err, null)
            } else {
                callback(null, JSON.stringify(data, null, 2));
            }
        });
    },
    readItem: function(params, endpoint, region, callback) {
        logger = winstonLogger.getLogger();

        AWS.config.update({
            endpoint: endpoint,
            region: region
        });
        var docClient = new AWS.DynamoDB.DocumentClient();
        docClient.get(params, function(err, data) {
            if (err) {
                callback(err, null);
            } else {

                callback(null, JSON.stringify(data, null, 2));
            }
        });


    },
    queryItem: function(params, endpoint, region, callback) {
        logger = winstonLogger.getLogger();
        AWS.config.update({
            endpoint: endpoint,
            region: region
        });

        var docClient = new AWS.DynamoDB.DocumentClient();
        docClient.query(params, function(err, data) {
            if (err) {
                callback(JSON.stringify(err, null, 2), null);
            } else {
                logger.info("Query succeeded.");
                callback(null, data.Items);
            }
        });
    },

    scanTable: function(params, endpoint, region, callback) {
        logger = winstonLogger.getLogger();

        AWS.config.update({
            endpoint: endpoint,
            region: region
        });

        var docClient = new AWS.DynamoDB.DocumentClient();
        docClient.scan(params, function(err, data) {
            if (err) {
                logger.error("Error in scanning");

                callback(JSON.stringify(err, null, 2), null);
            } else {
                logger.info("Scan succeeded.");
                callback(null, data.Items);
            }
        });
    },

    deleteItem: function(params,endpoint, region, callback){
        AWS.config.update({
            endpoint: endpoint,
            region: region
        });
        var docClient = new AWS.DynamoDB.DocumentClient();
        docClient.delete(params, function(err, data){
            if(err) {
                callback(err);
            }else {
                callback(null,data);
            }
        });
    },

    updateItem: function(params, endpoint, region, callback){
        AWS.config.update({
            endpoint: endpoint,
            region: region
        });
        var docClient = new AWS.DynamoDB.DocumentClient();
        docClient.update(params, function(err, data){
            if(err) {
                callback(err);
            } else {
                callback(null,data);
            }
        });
    },

    describeTable: function(params, endpoint,region,callback){
        AWS.config.update({
            endpoint: endpoint,
            region: region
        });
        var docClient = new AWS.DynamoDB();
        docClient.describeTable(params, function(err,data){
            if(err){
                callback(err);
            }else{
                callback(null,data);
            }
        });
    },

    createTable: function(params, endpoint, region,callback){
        AWS.config.update({
            endpoint: endpoint,
            region: region
        });
        var docClient = new AWS.DynamoDB();
        docClient.createTable(params, function(err,data){
            if(err){
                callback(err);
            }else{
                callback(null,data);
            }
        });
    }
}
