/**
 * Created by nikhil on 25/02/17.
 */


// { id: '12345',
//   workflow:
//    { actions: [ [Object], [Object], [Object], [Object] ],
//      Comment: 'Job WorkFlow in Amazon States Language using an AWS Lambda Function',
//      startAt: 'CreateJob',
//      states: [ [Object], [Object], [Object], [Object], [Object] ],
//      transitions: [ [Object], [Object], [Object], [Object], [Object], [Object] ] } }

'use strict'

var AWS = require('aws-sdk');

var path = require('path');
var stepFunctions=new AWS.StepFunctions({
    apiVersion: '2016-11-23'
});
const uuidV1 = require('uuid/v1');
var configenv = process.env.config;
var rootPath = process.env.root_path;
var fs=require('fs')

var resourceConfig = require(require(rootPath + "/config/configuration.js")(configenv));
var dbEndpoint = resourceConfig.dynamodbEndPoint;
var lambdaEndPoint = resourceConfig.lambdaEndpoint;
var AWSRegion = resourceConfig.lambdaRegion;
var db=require(rootPath+'/storage/util/dbUtil.js');
var taskTokenUtil = require(rootPath+'/aws/util/getTaskTokenForActivityWorker.js')
var winstonLogger = require(rootPath+'/log/util/winstonLogger.js');
var logger;

var constants = require(rootPath+'/config/constants.json');
var eventUtil = require(rootPath + '/event/util/eventUtil.js');
var events = require(rootPath + 'job/constants/validEvents.js');
var eventType = require(rootPath + 'event/eventType.js');
var stateActivityMap = require(rootPath +'/startup/stateActivityMap.json');
var metadataMap = require(rootPath +'/startup/metadataMap.json');

module.exports ={
    addTimeOut :function(event,context,callback){
        setTimeout(function () {
            console.log("Waiting for 10 seconds")
            callback(null);
        }, 10000)
    },
    setUpWorkFlowTable :function (event, context, callback) {
        var workflow={};
        workflow.id=uuidV1();
        workflow.workflow=event;
        console.log(workflow)

        var createParam= new db.createWriteItem(workflow,'Workflow');
        db.insertItem(createParam, resourceConfig.dynamodbEndPoint, resourceConfig.lambdaRegion, function(err, data) {

            console.log(err)
            console.log(data)

        });
    },
    setUpStateActivityTable : function(event,context,callback) {
        var activityNames = Object.keys(stateActivityMap);
        for(var i=0;i<activityNames.length;i++){
            var activityName = activityNames[i];
            var activityArn = stateActivityMap[activityName];
            var resource = "JOB";
            var state = activityName.substring(0,activityName.length-9);
            console.log(state);
            var stateActivityParam = {
                "state" : state,
                "resource_type" :resource,
                "activity_arn" : activityArn
            }
            var param = new db.createWriteItem(stateActivityParam, "StateActivityArns");
            db.insertItem(param, dbEndpoint, AWSRegion, function(err,data){
                if(err){
                    console.log(err);
                }else{
                    console.log(data);
                }
            });
        }
    },
    setUpActivityArnsTable : function(event,context,callback){
        var activityNames = Object.keys(stateActivityMap);
        for(var i=0;i<activityNames.length;i++){
            var activityArn = stateActivityMap[activityNames[i]];
            var stateActivityParam = {
                "activity_arn" : activityArn
            }
            var param = new db.createWriteItem(stateActivityParam, "ACTIVITY_ARNS");
            db.insertItem(param, dbEndpoint, AWSRegion, function(err,data){
                if(err){
                    console.log(err);
                }else{
                    console.log(data);
                }
            });
        }
    },
    setUpMetadataTable : function(event,context,callback) {
        var metaList = Object.keys(metadataMap);
        for(var i=0;i<metaList.length;i++){
            var key = metaList[i];
            var value = metadataMap[key];
            var resource = "JOB";
         
           // console.log(state);
            var metaDataParam = {
                "key" : key,
                "value":value

            }
            var param = new db.createWriteItem(metaDataParam, "Metadata");
            db.insertItem(param, dbEndpoint, AWSRegion, function(err,data){
                if(err){
                    console.log(err);
                }else{
                    console.log(data);
                }
            });
        }
    },
    createActivityArnTable : function(event,context,callback){
        var activityArnTable = constants.activityArnTableName;
        var param = {
            TableName: activityArnTable
        };
        db.describeTable(param, dbEndpoint,AWSRegion,function(err,data){
            if(err){
                console.log(err);
                console.log("Table does not exist. Creating table.");
                var params = {
                    AttributeDefinitions: [
                        {
                        AttributeName: "activity_arn", 
                        AttributeType: "S"
                    }
                    
                    ],
                    KeySchema: [
                        {
                        AttributeName: "activity_arn", 
                        KeyType: "HASH"
                    }
                    ],
                    ProvisionedThroughput: {
                    ReadCapacityUnits: 5, 
                    WriteCapacityUnits: 5
                    },
                    TableName: activityArnTable
                };
                db.createTable(params, dbEndpoint,AWSRegion,function(err,data){
                    if(err){
                        console.log("Error in creating table");
                        console.log(err);
                    }else{
                        console.log("Table created successfully");
                        console.log(data);
                    }
                });
            }else{
                console.log("Table exists");
                console.log(data);
            }
        });
    },
    createTaskTokensTable : function(event,context,callback){
        var taskTokensTable = constants.taskTokenTableName;
        var param = {
            TableName: taskTokensTable
        };
        db.describeTable(param, dbEndpoint,AWSRegion,function(err,data){
            if(err){
                console.log(err);
                console.log("Table does not exist. Creating table.");
                var params = {
                    AttributeDefinitions: [
                        {
                            AttributeName: "execution_id", 
                            AttributeType: "S"
                        },
                        {
                            AttributeName: "activity_arn", 
                            AttributeType: "S"
                        }
                    ],
                    KeySchema: [
                        {
                            AttributeName: "execution_id", 
                            KeyType: "HASH"
                        },
                        {
                            AttributeName: "activity_arn",
                            KeyType: "RANGE"
                        }
                    ],
                    ProvisionedThroughput: {
                    ReadCapacityUnits: 5,
                    WriteCapacityUnits: 5
                    },
                    TableName: taskTokensTable
                };
                db.createTable(params, dbEndpoint,AWSRegion,function(err,data){
                    if(err){
                        console.log("Error in creating table");
                        console.log(err);
                    }else{
                        console.log("Table created successfully");
                        console.log(data);
                    }
                });
            }else{
                console.log("Table exists");
                console.log(data);
            }
        });
    },
   createJobTable : function(event,context,callback){
        var jobTable = constants.jobTableName;
        var param = {
            TableName: jobTable
        };
        db.describeTable(param, dbEndpoint,AWSRegion,function(err,data){
            if(err){
                console.log(err);
                console.log("Table does not exist. Creating table.");
                var params = {
                    AttributeDefinitions: [
                        {
                            AttributeName: "id",
                            AttributeType: "S"
                        }
                    ],
                    KeySchema: [
                        {
                            AttributeName: "id",
                            KeyType: "HASH"
                        }
                    ],
                    ProvisionedThroughput: {
                    ReadCapacityUnits: 5,
                    WriteCapacityUnits: 5
                    },
                    TableName: jobTable
                };
                db.createTable(params, dbEndpoint,AWSRegion,function(err,data){
                    if(err){
                        console.log("Error in creating table");
                        console.log(err);
                    }else{
                        console.log("Table created successfully");
                        console.log(data);
                    }
                });
            }else{
                console.log("Table exists");
                console.log(data);
            }
        });
    },
   createEventTable : function(event,context,callback){
        var eventTableName = constants.eventTableName;
        var param = {
            TableName: eventTableName
        };
        db.describeTable(param, dbEndpoint,AWSRegion,function(err,data){
            if(err){
                console.log(err);
                console.log("Table does not exist. Creating table.");
                var params = {
                    AttributeDefinitions: [
                        {
                            AttributeName: "id",
                            AttributeType: "S"
                        }
                    ],
                    KeySchema: [
                        {
                            AttributeName: "id",
                            KeyType: "HASH"
                        }
                    ],
                    ProvisionedThroughput: {
                    ReadCapacityUnits: 5,
                    WriteCapacityUnits: 5
                    },
                    TableName: eventTableName
                };
                db.createTable(params, dbEndpoint,AWSRegion,function(err,data){
                    if(err){
                        console.log("Error in creating table");
                        console.log(err);
                    }else{
                        console.log("Table created successfully");
                        console.log(data);
                    }
                });
            }else{
                console.log("Table exists");
                console.log(data);
            }
        });
    },
   createStateActivityARNsTable : function(event,context,callback){
        var stateActivityARNsTable = constants.statesActivityArnMappingTableName;
        var param = {
            TableName: stateActivityARNsTable
        };
        db.describeTable(param, dbEndpoint,AWSRegion,function(err,data){
            if(err){
                console.log(err);
                console.log("Table does not exist. Creating table.");
                var params = {
                    AttributeDefinitions: [
                        {
                            AttributeName: "state",
                            AttributeType: "S"
                        },
                         { AttributeName: "resource_type", AttributeType: "S" }
                    ],
                    KeySchema: [
                        {
                            AttributeName: "state",
                            KeyType: "HASH"
                        },
                         { AttributeName: "resource_type", KeyType: "RANGE" }
                    ],
                    ProvisionedThroughput: {
                    ReadCapacityUnits: 5,
                    WriteCapacityUnits: 5
                    },
                    TableName: stateActivityARNsTable
                };
                db.createTable(params, dbEndpoint,AWSRegion,function(err,data){
                    if(err){
                        console.log("Error in creating table");
                        console.log(err);
                    }else{
                        console.log("Table created successfully");
                        console.log(data);
                    }
                });
            }else{
                console.log("Table exists");
                console.log(data);
            }
        });
    },
   createWorkflowTable : function(event,context,callback){
        var workflowTable = constants.workFlowTableName;
        var param = {
            TableName: workflowTable
        };
        db.describeTable(param, dbEndpoint,AWSRegion,function(err,data){
            if(err){
                console.log(err);
                console.log("Table does not exist. Creating table.");
                var params = {
                    AttributeDefinitions: [
                        {
                            AttributeName: "id",
                            AttributeType: "S"
                        }
                    ],
                    KeySchema: [
                        {
                            AttributeName: "id",
                            KeyType: "HASH"
                        }
                    ],
                    ProvisionedThroughput: {
                    ReadCapacityUnits: 5,
                    WriteCapacityUnits: 5
                    },
                    TableName: workflowTable
                };
                db.createTable(params, dbEndpoint,AWSRegion,function(err,data){
                    if(err){
                        console.log("Error in creating table");
                        console.log(err);
                    }else{
                        console.log("Table created successfully");
                        console.log(data);
                    }
                });
            }else{
                console.log("Table exists");
                console.log(data);
            }
        });
    },
    createMetaDataTable : function(event,context,callback){
        var metadataTable = constants.metadata;
        var param = {
            TableName: metadataTable
        };
        db.describeTable(param, dbEndpoint,AWSRegion,function(err,data){
            if(err){
                console.log(err);
                console.log("Table does not exist. Creating table.");
                var params = {
                    AttributeDefinitions: [
                        {
                            AttributeName: "key",
                            AttributeType: "S"
                        }
                    ],
                    KeySchema: [
                        {
                            AttributeName: "key",
                            KeyType: "HASH"
                        }
                    ],
                    ProvisionedThroughput: {
                    ReadCapacityUnits: 5,
                    WriteCapacityUnits: 5
                    },
                    TableName: metadataTable
                };
                db.createTable(params, dbEndpoint,AWSRegion,function(err,data){
                    if(err){
                        console.log("Error in creating table");
                        console.log(err);
                    }else{
                        console.log("Table created successfully");
                        console.log(data);
                    }
                });
            }else{
                console.log("Table exists");
                console.log(data);
            }
        });
    }

}
