#!/bin/ksh

exec 1>startUp.log
exec 2>&1
root=${root_path}

cd $root/job/serverless
serverless deploy

sls deploy stepf 
log=`cat $root/startup/startUp.log|sed -n -e '/Deployed Activity ARNs/,$p' |sed 1d|sed '/^$/d'|awk -F": " -v quotec="\"," -v quote="\"" -v colon=": " '{print quote $1 quote ,colon  quote $2 quotec}' `
echo "log variable created"
echo { "$log"}|sed 's/,}/ }/g' > $root/startup/stateActivityMap.json


meta=`grep -i "JobWorkFlowStage1" $root/startup/startUp.log |awk -F": " -v quotec="\"," -v quote="\"" -v colon=": " '{print quote $1 quote ,colon quote $2 quotec}' `
echo { "$meta"}|sed 's/,}/ }/g' > $root/startup/metadataMap.json


echo "State Activity map created"
#echo {"$log"}
# tmp=`echo "$(cat $root/startup/dummy.txt)"|sed -n -e '/ARNs/,$p' |sed 1d|awk -F": " -v quote="\"" -v colon=": " '{print quote $1 quote ,colon  quote $2 quote}'`

# echo {"$tmp"}

cd $root

serverless invoke local --function createWorkflowTable
serverless invoke local --function createStateActivityARNsTable
serverless invoke local --function createActivityArnTable
serverless invoke local --function createJobTable
serverless invoke local --function createEventTable
serverless invoke local --function createTaskTokensTable
serverless invoke local --function createMetaDataTable
#serverless invoke local --function addTimeOut
sleep 10
echo "After 10 seconds of sleep"
serverless invoke local --function setUpWorkFlowTable --path=$root/startup/workflow.json
serverless invoke local --function setUpStateActivityTable 
serverless invoke local --function setUpActivityArnsTable
serverless invoke local --function setUpMetadataTable
